#include <linux/fs.h>
#include <asm/segment.h>
#include <asm/uaccess.h>
#include <linux/buffer_head.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/uaccess.h>
#include <linux/sched.h>

/*
 * This small kernel module gives you 2 chardev that you can make
 * appear in /dev with `make mknod`.
 *
 * /dev/d_state is where your application can write. Everything
 * that is read/write from/to this device will be redirected to the normal file
 * /tmp/d-state.
 *
 * /dev/d_state_control is how you can control the knobs. It accepts a
 * couple of 4-char commands:
 *
 *   - echo blck > /dev/d_state_control
 *   - echo ublk > /dev/d_state_control
 *
 * The first command will tell /dev/d_state to block and put the
 * process in D state, while the second command will unblock it.
 */

MODULE_LICENSE("Dual BSD/GPL");

static void d_state_exit(void);
static int d_state_init(void);

/* Declaration of the init and exit functions */
module_init(d_state_init);
module_exit(d_state_exit);

static int control_open(struct inode *inode, struct file *filp);
static int control_release(struct inode *inode, struct file *filp);
static ssize_t control_read(struct file *filp, char *buf, size_t count, loff_t *f_pos);
static ssize_t control_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos);

struct file_operations control_fops = {
				       .read = control_read,
				       .write = control_write,
				       .open = control_open,
				       .release = control_release
};

static int write_open(struct inode *inode, struct file *filp);
static int write_release(struct inode *inode, struct file *filp);
static ssize_t write_read(struct file *filp, char *buf, size_t count, loff_t *f_pos);
static ssize_t write_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos);

struct file_operations write_fops = {
				     .read = write_read,
				     .write = write_write,
				     .open = write_open,
				     .release = write_release
};

static int control_major = 60;
static int write_major = 61;

static int control_registered = 0;
static int write_registered = 0;
static int out_open = 0;

static DECLARE_WAIT_QUEUE_HEAD(wq);

static atomic_t should_block = ATOMIC_INIT(0);

static struct file *out;

static int d_state_init(void)
{
	mm_segment_t oldfs;

	if (register_chrdev(control_major, "d_state_control", &control_fops) < 0) {
		printk(KERN_DEBUG "d_state: cannot obtain major number %d\n", control_major);
		d_state_exit();
		return -1;
	}
	control_registered = 1;

       	if (register_chrdev(write_major, "d_state_write", &write_fops) < 0) {
		printk(KERN_DEBUG "d_state: cannot obtain major number %d\n", write_major);
		d_state_exit();
		return -1;
	}
	write_registered = 1;

	oldfs = get_fs();
	set_fs(get_ds());
	out = filp_open("/tmp/d-state", O_CREAT|O_APPEND|O_WRONLY, 0666);
	set_fs(oldfs);
	if (IS_ERR(out)) {
		d_state_exit();
		return -PTR_ERR(out);
	}
	out_open = 1;

	printk(KERN_DEBUG "Inserting d_state module\n"); 
	return 0;
}

static void d_state_exit(void)
{
	if (out_open)
		filp_close(out, 0);

	if (control_registered)
		unregister_chrdev(control_major, "d_state_control");

	if (write_registered)
		unregister_chrdev(write_major, "d_state_write");

	printk(KERN_DEBUG "Removing d_state module\n");
}

static int control_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int control_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static ssize_t control_read(struct file *filp, char *buf, size_t count, loff_t *f_pos)
{
	return 0;
}

static ssize_t control_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos)
{
	char command[6] = {0, 0, 0, 0, 0, 0};

	if (copy_from_user((void *) command, buf, 5) > 0) {
		printk(KERN_DEBUG "too much was read");
		return -EOPNOTSUPP;
	}

	printk(KERN_DEBUG "read %s", command);

	if (strncmp(command, "blck\n", 5) == 0) {
		if (!atomic_read(&should_block)) {
			atomic_set(&should_block, 1);
			printk(KERN_DEBUG "Set it to block");
		} else {
			printk(KERN_DEBUG "Already set to block");
		}
	}

	if (strncmp(command, "ublk\n", 5) == 0) {
		if (atomic_read(&should_block)) {
			atomic_set(&should_block, 0);
			wake_up(&wq);
			printk(KERN_DEBUG "Set it to unblock");
		} else {
			printk(KERN_DEBUG "Already set to unblock");
		}
	}

	return 5;
}

static int write_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int write_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static ssize_t write_read(struct file *filp, char *buf, size_t count, loff_t *f_pos)
{
	return kernel_read(out, buf, count, f_pos);
}

static ssize_t write_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
{
	int ret;
	char *buffer;
	loff_t pos = out->f_pos;

	if (atomic_read(&should_block)) {
		wait_event(wq, should_block.counter == 0);
	}

	buffer = kmalloc(count, GFP_KERNEL);
	if (buffer == NULL)
		return -ENOMEM;

	if (copy_from_user(buffer, buf, count)) {
		kfree(buffer);
		return -EIO;
	}

	ret = kernel_write(out, buffer, count, &pos);

	kfree(buffer);

	return ret;
}
