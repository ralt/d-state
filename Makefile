obj-m += d-state.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

install:
	sudo insmod d-state.ko

uninstall:
	sudo rmmod d_state

mknod:
	sudo mknod /dev/d_state-control c 60 0
	sudo chmod 666 /dev/d_state-control

	sudo mknod /dev/d_state c 61 0
	sudo chmod 666 /dev/d_state
