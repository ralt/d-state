# d-state

A very small kernel module whose sole purpose is to be able to test
processes misbehaving when they end up in D state.

The bug that started this was in a clustered database, where the
quorum crashed if one of the servers ended up in D state.

## Installation

```bash
make
make mknod # creates /dev/d_state and /dev/d_state-control
make install
```

To get rid of the module, either `make uninstall`, or reboot.

Note that `/dev/d_state` and `/dev/d_state-control` will be gone after
a reboot, and you will need to run `make mknod` again.

## Usage

Whatever you write to `/dev/d_state` is going to be forwarded to
`/tmp/d-state`. This lets you verify that the application is working
appropriately.

When you want to put the process writing to `/dev/d_state` in D state,
run the following:

```bash
echo blck > /dev/d_state-control
```

The processes writing to `/dev/d_state` will immediately end up in D
state.

When you want to unlock these processes, you can run the following:

```bash
echo ublk > /dev/d_state-control
```

## License

Dual BSD/GPL license.

## Author

Florian Margaine <florian@margaine.com>
